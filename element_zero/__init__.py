import importlib

# import and reload all libraries before reloading cogs
from element_zero.util import database, argparse, errors, logging

importlib.reload(errors)
importlib.reload(database)
importlib.reload(argparse)
importlib.reload(logging)

# import and reload all cogs
from element_zero.cogs import emoji

importlib.reload(emoji)


def setup(liara):
    liara.add_cog(emoji.Emoji(liara))
